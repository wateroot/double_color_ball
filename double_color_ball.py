
#-*- encoding=gb2312 -*-
'''prognosticate'''

import os
import sys
import re
import random as rd

str_patt = r'[ ]{4}([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) : ([0-9]{2})'
str_patt_1 = r'[ ]{4}([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) ([0-9]{2}) : ([0-9]{2})    #([0-9]{7})'

dct_red = {}
dct_blu = {}

lst_red = []
lst_blu = []

period = 2009
total_cnt = 0

def init_dict():
    for i in range(1, 34):
        dct_red[i] = 0
    for i in range(1, 17):
        dct_blu[i] = 0
    return

def calc_cnt(file_name):
    global total_cnt
    global period
    db = open(file_name)
    for ln in db:
        if 0 != total_cnt:
        	  m = re.search(str_patt, ln)
        else:
            m = re.search(str_patt_1, ln)
            if None != m:
                total_cnt = 1
                period = m.group(8)
                continue
        if None != m:
            cnt = 0
            for x in m.groups():
                cnt = cnt + 1
                if cnt <= 6:
                    #print ln
                    dct_red[int(x)] = dct_red[int(x)] + 1
                else:
                    dct_blu[int(x)] = dct_blu[int(x)] + 1
    return

def sort_dict():
    global dct_red
    global dct_blu
    for item in dct_red.items():
        lst_red.append((item[1], item[0]))
        lst_red.sort() #sort by inc
    for item in dct_blu.items():
        lst_blu.append((item[1], item[0]))
        lst_blu.sort() #sort by inc
    del dct_red
    del dct_blu
    return

def prognosticate():
    print '%02d %02d %02d %02d %02d %02d : %02d' % \
        (lst_red[0][1], lst_red[1][1], lst_red[2][1], lst_red[3][1], \
        lst_red[4][1], lst_red[5][1], lst_blu[0][1])
    '''
    print '%02d %02d %02d %02d %02d %02d : %02d' % \
        (lst_red[12][1], lst_red[13][1], lst_red[14][1], \
        lst_red[15][1], lst_red[16][1], lst_red[17][1], lst_blu[7][1])
    lst_red.reverse() #sort by dec
    lst_blu.reverse() #sort by dec
    print '%02d %02d %02d %02d %02d %02d : %02d' % \
        (lst_red[0][1], lst_red[1][1], lst_red[2][1], lst_red[3][1], \
        lst_red[4][1], lst_red[5][1], lst_blu[0][1])
    '''

def random_select():
    num_set = []
    for i in range(1, 34):
        num_set.append(i)
    red = rd.sample(num_set, 6)
    num_set = []
    for i in range(1, 17):
        num_set.append(i)
    blu = rd.sample(num_set, 1)
    del num_set
    print '%02d %02d %02d %02d %02d %02d : %02d' % \
        (red[0], red[1], red[2], red[3], red[4], red[5], blu[0])

if __name__ == '__main__':
    #initiate all the data.
    if len(sys.argv) < 2:
        print 'prog.py <file_name> [stats|random]'
        exit()

    init_dict()
    calc_cnt(sys.argv[1])
    sort_dict()

    #print all the statistics data.
    if len(sys.argv) > 2:
        if 'stats' == sys.argv[2]:
            print '-[* red ball probability *]-'
            #print lst_red
            for x in lst_red:
                print 'red_ball: [%02d], frequency: [%d]' % (x[1], x[0])
            print '\n-[* blue ball probability *]-'
            #print lst_blu
            for x in lst_blu:
                print 'blue_ball: [%02d], frequency: [%d]' % (x[1], x[0])
        elif 'random' == sys.argv[2]:
            print u'\n���ں���Ԥ��:', (int(period)+1)
            random_select() #prognosticate the next number by random.
    else:
        print u'\n���ں���Ԥ��:', (int(period)+1)
        prognosticate() #prognosticate the next numbers.
        print '\n\n'
else:
    print 'input error.'
